import { ExecutorContext, ProjectsConfigurations } from '@nrwl/devkit';
import { exec } from 'node:child_process';
import * as util from 'util';

const execute = util.promisify(exec)

export function getProjectRoot(context: ExecutorContext){
  const projName = context.projectName
  const projects = context.workspace.projects
  return projects[projName].root
}

export async function getPath(){
  const result = (await commandInShell('poetry env info -p', true)).stdout
  return result.trimEnd()
}

export default async function commandInShell(command: string, quiet = false) {
    try {
      const success = await execute(command)
      if (!quiet) {
        console.log(success.stdout)
      }
      return {
        success: true,
        stdout: success.stdout
      }
    } catch(err)  {
      console.log(err.stdout)
      return {
        success: false,
        stdout: err.stdout
      }
    }
}

export function getMockedContext() {
  const workspace = {
    projects: {}
  } as ProjectsConfigurations
  const  context: ExecutorContext = { 
    targetName: "name",
    projectName: "name",
    workspace: workspace} as ExecutorContext
  context.workspace.projects = {"name": {root: "root"}} 
  return context
}


