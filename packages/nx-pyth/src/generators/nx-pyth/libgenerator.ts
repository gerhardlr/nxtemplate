import {
  addProjectConfiguration,
  formatFiles,
  Tree,
} from '@nrwl/devkit';
import { NxPythGeneratorSchema } from './schema';
import { normalizeOptions, addFiles, updatePoetry, generateProjectConfig} from './lib'
import commandInShell from '../../executors/lib/utils';



export default async function (tree: Tree, options: NxPythGeneratorSchema) {
  const normalizedOptions = normalizeOptions(tree, options, "library");
  const projectConfiguration = generateProjectConfig(normalizedOptions, "library")
  addProjectConfiguration(tree, normalizedOptions.projectName, projectConfiguration);
  addFiles(tree, normalizedOptions, "library");
  await updatePoetry(tree,normalizedOptions,"library")
  await formatFiles(tree);
  await commandInShell(`poetry install`)
}
