export interface NxPythGeneratorSchema {
  name: string;
  tags?: string;
  directory?: string;
  fastapi?: boolean;
  websocket?: boolean;
}