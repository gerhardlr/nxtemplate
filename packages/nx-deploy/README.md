# nx-deploy

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build nx-deploy` to build the library.

## Running unit tests

Run `nx test nx-deploy` to execute the unit tests via [Jest](https://jestjs.io).
