import * as fs from 'fs/promises'
import * as path from 'path';
import * as YAML from 'yaml';


export interface AppObject {
    name: string
    path: string
    replicaCount: number
    port : number
}

interface App {
    path: string
    replicaCount: number
    port : number
}

export type Apps = Map<string,App>

async function getvalues(root: string, project: string):Promise<any>  {
    const valuesFilePath = path.join(root,`${project}/values.yaml`)
    const valuesFile = await fs.readFile(valuesFilePath)
    return YAML.parse(valuesFile.toString())
}

async function writeValues(values: any,root: string, project: string):Promise<any>  {
    const valuesFilePath = path.join(root,`${project}/values.yaml`)
    let valuesString = YAML.stringify(values,{
        nullStr:"",
    })
    const pattern = /(\n)(?=\w)/gm
    valuesString = valuesString.replace(pattern,"\n\n")
    await fs.writeFile(valuesFilePath,valuesString)
}

function mapToArray<V>(inputMap: Map<string,V>){
    const mapArray: [string, V][] = []
    inputMap.forEach((value, key) => {
        mapArray.push([key,value])
    }) 
    return mapArray
}

function merge<T>(objectA: Map<string, T>, objectB: Map<string, T>){
    const arrayObjectA = mapToArray(objectA)
    const arrayObjectB = mapToArray(objectB)
    return new Map([...arrayObjectA, ...arrayObjectB])
}


export async function addBackend(backend: AppObject, root: string, project: string) {
    const values = await getvalues(root, project)
    let backendObjects = values['backends']
    if (backendObjects === null) {
        backendObjects = []
    }
    const backends = asApps(backendObjects as AppObject[])
    const updated = merge(backends, toMap(backend))
    values['backends'] = asAppObjects(updated)
    await writeValues(values,root,project)
}

export async function addFrontend(frontend: AppObject, root: string, project: string) {
    const values = await getvalues(root, project)
    let frontendObjects = values['frontends']
    if (frontendObjects === null) {
        frontendObjects = []
    }
    const frontends = asApps(frontendObjects as AppObject[])
    const updated = merge(frontends, toMap(frontend))
    values['frontends'] = asAppObjects(updated)
    await writeValues(values,root,project)
}

function toMap({name, ...data}: AppObject):Map<string, any> {
    return new Map([[name,data]])
}

function asApps(appObjects: AppObject[]) {
    const appArray: [string, App][] = []
    appObjects.map(appObject => {
        const {name, ...app} = appObject
        appArray.push([appObject.name, app])
    })
    return new Map<string, App>(appArray)
}

function asAppObjects(apps: Apps){
    const appObjects: AppObject[] = []
    apps.forEach((app,name) => appObjects.push({name, ...app}))
    return appObjects
}

