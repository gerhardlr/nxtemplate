import { AddAppExecutorSchema } from './schema';
import backendExecutor from './backend-executor';
import frontendExecutor from './frontend-executor';
import { getMockedContext, MockedFolder, mockedProperties } from './lib/utils';
import { assert } from 'console';

function mockFn(command, callback)  {
  callback(null, {stdout: "stdout"}, {stderr: "stderr"})
}
jest.mock('node:child_process', () => { return {exec: mockFn}})
const options: AddAppExecutorSchema = {name: mockedProperties.testAppName};

describe('Add backend/frontend Executor', () => {

  const  context = getMockedContext()
  const mockedFolder = new MockedFolder()
  beforeEach(async () => {
    await mockedFolder.setup()
  })

  afterEach(async () => {
    await mockedFolder.teardown()
  })

  it('works', () => {
    const foo = 'bar'
  })

  it('can add a backend', async () => {
    await backendExecutor(options, context);
    const values = await mockedFolder.getValuesFile()
    expect(values['backends']).toStrictEqual([{
      name: mockedProperties.testAppName,
      path: mockedProperties.testAppName,
      replicaCount: 1,
      port: 5000,
    }])
    const projectData = await mockedFolder.getProjectJsonFile()
    expect(projectData.implicitDependencies).toStrictEqual([mockedProperties.testAppName])
  });

  it('can add a frontend', async () => {
    await frontendExecutor(options, context);
    const values = await mockedFolder.getValuesFile()
    expect(values['frontends']).toStrictEqual([{
      name: mockedProperties.testAppName,
      path: mockedProperties.testAppName,
      replicaCount: 1,
      port: 5000,
    }])
    const projectData = await mockedFolder.getProjectJsonFile()
    expect(projectData.implicitDependencies).toStrictEqual([mockedProperties.testAppName])
  });
});
