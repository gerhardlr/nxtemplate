import {
  addProjectConfiguration,
  formatFiles,
  generateFiles,
  Tree,
} from '@nx/devkit';
import * as path from 'path';
import { CreateGeneratorSchema } from './schema';

export async function createGenerator(
  tree: Tree,
  options: CreateGeneratorSchema
) {
  const name = options.name
  const projectRoot = `apps/${name}`;
  addProjectConfiguration(tree, name, {
    root: projectRoot,
    projectType: 'application',
    sourceRoot: `${projectRoot}/${name}`,
    implicitDependencies: [],
    targets: {
      addBackendTo: {
        "executor": "@nxtemplate/nx-deploy:addBackend"
      },
      addFrontendTo: {
        "executor": "@nxtemplate/nx-deploy:addFrontend"
      },
      build: {
        "dependsOn": [
          "^build"
        ],
        executor: "@nx-tools/nx-container:build",
        configurations: {
          dev: {
            metadata: {
              images: [
                `local/${name}`
              ]
            }
          },
          ci: {
            metadata: {
              images: [
                `registry.gitlab.com/gerhardlr/${name}`
              ]
            }
          }
        },
        options: {
          engine: "docker",
          load: false,
          tags: [
            "type=schedule",
            "type=ref,event=branch",
            "type=ref,event=tag",
            "type=ref,event=pr",
            "type=sha,prefix=sha-"
          ]
        }
      },
      deploy: {
        dependsOn: [
          "build"
        ],
        executor: "nx:run-commands",
        outputs: [
          `{workspaceRoot}/dist/${name}/`
        ],
        defaultConfiguration: "dev",
        options: {
          "cwd": `apps/${name}`
        },
        configurations: {
          dev: {
            command: `helm upgrade ${name} ${name} -n ${name}--install --atomic  --create-namespace --values .variants/dev.yaml`
          },
          template: {
            commands: [
              `mkdir -p ../../dist/${name}/`,
              `helm template $/ --values .variants/dev.yaml > ../../dist/${name}/deployment.yaml`,
              `echo template stored in ../../dist/${name}/deployment.yaml`
            ]
          }
        }
      }
    }
  });
  generateFiles(tree, path.join(__dirname, 'files'), projectRoot, options);
  await formatFiles(tree);
}

export default createGenerator;
